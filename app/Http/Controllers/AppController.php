<?php
namespace App\Http\Controllers;

use App\Filial;
use App\File;
use App\Vendor;
use Illuminate\Support\Facades\Input;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;


class AppController extends Controller{

    public function acesso($code){

        $result = array();

        $vendedor = Vendor::where('code', $code)->get();
        if($vendedor->isEmpty()){

            return json_encode(array('error' => 'código inválido'));
        }else{

            $filial = Filial::find($vendedor[0]->filial_id);
            $file   = File::where('filial_id', $filial->id)->get();
            $img    = (preg_replace('/.[^.]*$/', '', $file[0]->file)) . '.jpg';

            $result['vendedor']['code'] = $vendedor[0]->code;
            $result['vendedor']['name'] = $vendedor[0]->name;
            $result['filial']['code']   = $filial->code;
            $result['filial']['name']   = $filial->name;
            $result['file']['path']     = $file[0]->file;
            $result['file']['image']    = $img;

            return json_encode($result);
        }
    }

    public function v2($code){
        $vendedor = Vendor::where('code', $code)->get();
        if($vendedor->isEmpty()){

            return json_encode(array('error' => 'código inválido'));
        }else{

            $filial = Filial::find($vendedor[0]->filial_id);
            $file   = File::where('filial_id', $filial->id)->where('ativo', 1)->get();

            $result['vendedor']['code'] = $vendedor[0]->code;
            $result['vendedor']['name'] = $vendedor[0]->name;
            $result['filial']['code']   = $filial->code;
            $result['filial']['name']   = $filial->name;

            for($i = 0; $i < count($file); $i++){
                $fileInfo   = pathinfo($file[$i]->file);
                $img    = $fileInfo['dirname'].'/thumb_'.$fileInfo['filename'].'.jpg';

                $files[$i]['name']     = $file[$i]->name;
                $files[$i]['path']     = $file[$i]->file;
                $files[$i]['image']    = $img;
            }
            $result['file']     = $files;

            return json_encode($result);
        }
    }


}