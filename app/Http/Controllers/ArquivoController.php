<?php
namespace App\Http\Controllers;

use App\Filial;
use App\File;
use Illuminate\Support\Facades\Input;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;


class ArquivoController extends Controller{

    public function create() {
        $filials = Filial::lists('name', 'id');
        return view('files.create')->with('filials', $filials);
    }

    protected function save($arquivo) {

        $request = Request::all();

        if($arquivo->id != 0) {
            $rules = [
                'name'          => 'required',
                'description'   => 'required',
                'filial_id'     => 'required',
                'arquivo'       => 'mimes:pdf,jpg,jpeg,mp4'
            ];
        }else{
            $rules = [
                'name'          => 'required',
                'description'   => 'required',
                'filial_id'     => 'required',
                'arquivo'       => 'required|mimes:pdf,jpg,jpeg,mp4'
            ];
        }

        $valid = Validator::make($request, $rules);

        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid->messages());
        }else {

            if(Request::hasFile('arquivo')) {

                $file           = Request::file('arquivo');
                $ext            = $file->getClientOriginalExtension();

                $date           = new \DateTime();
                $filename       = 'ibiapp'.$date->format('Hmi').'.'.$ext;
                $storageFolder  = '/pdf/'.$date->format('m/d').'/';
                $storagePath    = public_path().$storageFolder;



                $file->move($storagePath, $filename);

                $this->pdfThumbGenerate($storagePath, $filename);

                $arquivo->file  = $storageFolder.$filename;
            }


            $arquivo->name          = Input::get('name');
            $arquivo->description   = Input::get('description');
            $arquivo->filial_id     = Input::get('filial_id');
            $arquivo->ativo         = Input::get('ativo') == '1' ? '1' : '0';
            $arquivo->save();

            Session::flash('message', 'Arquivo salvo com sucesso!');
            return redirect('/arquivos');
        }
    }

    public function store() {
        return $this->save(new File());
    }

    public function index() {
        $files = File::all();
        foreach($files as $f) {
            $fileInfo   = pathinfo($f->file);
            $f->pdf     = $f->file;
            $f->img     = $fileInfo['dirname'].'/thumb_'.$fileInfo['filename'].'.jpg';
        }
        return view('files.list')->with('files', $files);
    }

    public function show() {

    }

    public function edit($id) {
        $filials    = Filial::lists('name', 'id');
        $file = File::findOrFail($id);
        return view('files.edit')->with('arquivo', $file)->with('filials', $filials);
    }

    public function update($id) {
        return $this->save(File::findOrFail($id));
    }

    public function destroy($id) {

        $file = File::find($id);

        if(File::exists(public_path().$file->file)) {

            $pdf = $file->file;
            $img = (preg_replace('/.[^.]*$/', '', $file->file)).'jpg';

            File::destroy($pdf);
            File::destroy($img);
        }

        $file->delete();

        // redirect
        Session::flash('message', 'Arquivo excluido com sucesso!');
        return redirect('/arquivos');
    }

    public function upload() {

        $file           = Request::file('arquivo');
        $storagePath    = public_path().'/pdf/tmp/';
        $fileName       = $file->getClientOriginalName();
        $file->move($storagePath, $fileName);

        return $this->pdfThumbGenerate($storagePath, $fileName);

    }

    private function pdfThumbGenerate($storagePath, $file) {

        $fileName   = pathinfo($file, PATHINFO_FILENAME);
        $imageName  = 'thumb_'.$fileName.'.jpg';

        if(pathinfo($file, PATHINFO_EXTENSION) == 'mp4') {
            $im     = new \Imagick(public_path().'/video-icon.jpg');
        } else {
            $im     = new \Imagick($storagePath."/".$file."[0]");
        }
        $img    = Image::make($im);
        $img->widen(200)->save($storagePath.$imageName);

        return $imageName;
    }
}