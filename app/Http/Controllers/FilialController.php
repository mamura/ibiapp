<?php
namespace App\Http\Controllers;

use App\Filial;
use Illuminate\Support\Facades\Input;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;


class FilialController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * CRUD
     * C - Create
     *
     * Exibe o formulário de criação de Filial
     */
    public function create(){

        return view('filials.create');
    }

    /**
     * Salva uma nova Filial
     */
    protected function save($filial){

        $request =Request::all();
        $valid = Validator::make($request, [
            'code' => 'required|unique:filials,id'.$filial->id != 0 ? ",$filial->id" : ''.'|max:4',
            'name' => 'required',
        ]);

        if ($valid->fails()) {
            //dd($valid->messages());
            return redirect()->back()->withErrors($valid->messages());
        }else {
            $filial->code = Input::get('code');
            $filial->name = Input::get('name');

            $filial->save();

            Session::flash('message', 'Filial salva com sucesso!');
            return redirect('/filiais');
        }
    }

    public function store(){
        return $this->save(new Filial());
    }

    /**
     * CRUD
     * R - Read
     *
     * Exibe a lista de Filiais
     */
    public function index(){
        $filials = Filial::all();
        return view('filials.list')->with('filials', $filials);
    }

    /**
     * Exibe uma Filial.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * CRUD
     * U - Update
     *
     * Mostra o formulário para editar uma Filial.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $filial = Filial::findOrFail($id);

        return view('filials.edit')->with('filial', $filial);
    }

    /**
     * Atualiza uma Filial.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        return $this->save(Filial::findOrFail($id));
    }

    /**
     * CRUD
     * D - Delete
     *
     * Remove uma Filial.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $filial = Filial::find($id);
        $filial->delete();

        // redirect
        Session::flash('message', 'Filial excluida com sucesso!');
        return redirect('/filiais');
    }
}