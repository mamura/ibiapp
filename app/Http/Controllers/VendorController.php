<?php
namespace App\Http\Controllers;

use App\Filial;
use App\Vendor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;


class VendorController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create() {
        $filials = Filial::lists('name', 'id');
        return view('vendors.create')->with('filials', $filials);
    }

    protected function save($vendor) {
        $request = Request::all();

        $valid = Validator::make($request, [
            'code'      => 'required|unique:vendors,id'.$vendor->id != 0 ? ",$vendor->id" : ''.'|max:11',
            'name'      => 'required',
            'filial_id' => 'required'
        ]);

        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid->messages());
        }else {

            $vendor->code       = Input::get('code');
            $vendor->name       = Input::get('name');
            $vendor->filial_id  = Input::get('filial_id');

            $vendor->save();

            Session::flash('message', 'Vendedor salvo com sucesso!');
            return redirect('/vendedores');
        }
    }

    public function store() {
        return $this->save(new Vendor());
    }

    public function index() {

        $vendedores = Vendor::search(Input::all(), DB::table('vendors'));

        $vendors = $vendedores->paginate(15);
        $filials    = Filial::lists('name', 'id');
        return view('vendors.list')->with('vendors', $vendors)->with('filials', $filials);
    }

    public function show() {

    }

    public function edit($id) {
        $filials    = Filial::lists('name', 'id');
        $vendor     = Vendor::findOrFail($id);
        return view('vendors.edit')->with('vendor', $vendor)->with('filials', $filials);
    }

    public function update($id) {
        return $this->save(Vendor::findOrFail($id));
    }

    public function destroy($id) {

        $vendor = Vendor::find($id);
        $vendor->delete();

        // redirect
        Session::flash('message', 'Vendedor excluido com sucesso!');
        return redirect('/vendedores');
    }
}