<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth'      => 'Auth\AuthController',
	'password'  => 'Auth\PasswordController',
]);

Route::post('/login', 'AuthController@loginApp');

Route::resource('filiais', 'FilialController');
Route::resource('vendedores', 'VendorController');
Route::resource('arquivos', 'ArquivoController');
Route::post('arquivos/upload', 'ArquivoController@upload');

Route::get('app/acesso/{code}', 'AppController@acesso');
Route::get('app/v2/{code}', 'AppController@v2');