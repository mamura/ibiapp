<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->composer('partials.menu_interno', function($view){
			$sumVendors = \App\Vendor::count();
			$sumFilials = \App\Filial::count();
			$sumFiles 	= \App\File::count();


			$view->with('totalVendedores', $sumVendors)
				->with('totalFiliais', $sumFilials)
				->with('totalArquivos', $sumFiles);
		});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
