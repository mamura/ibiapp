<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vendor extends Model {

	public static function search(array $data = array(), $query){

        $data       = array_filter($data, 'strlen');


        foreach($data as $field => $value){
            if(isset($data[$field]) && $field != 'page'){

                if($field == 'name'){
                    $query->where($field, 'like', '%'.$data[$field].'%');
                }else{
                    $query->where($field, $data[$field]);
                }


            }
        }
        return $query;
    }

}
