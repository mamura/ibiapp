/**
 * Created by marcio.mota on 04/09/2015.
 */
$('.alert .close').click( function(e){
    e.preventDefault();
    $(this).parents('.alert').fadeOut(300);
});

jQuery(window).load(function() {
    jQuery('.number').mask('000000');

    jQuery('.multiselect').multiselect({
        maxHeight: 300,
        buttonClass: 'dropdown-toggle btn btn-default'
    });

    jQuery('#fileupload').fileupload({
        url: '/arquivos/upload',

        formData: {_token: jQuery('#fileupload').data('token')},
        done: function (e, data) {
            $('<img/>').attr('src', '/pdf/tmp/'+data.result).appendTo('#files');
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    })
});

