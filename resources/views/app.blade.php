<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>IbiApp</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap-multiselect.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/bower_components/jquery-file-upload/css/jquery.fileupload.css') }}"/>

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="dashboard">
    <div class="wrapper">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 logo">
                        <a href="{{ url('/home') }}">
                            <img alt="" src="{{ asset('/images/logo-white.png') }}">
                        </a>
                        <h1 class="sr-only">Ibiapina - IbiApp</h1>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-10">
                                <!-- nav class="navbar navbar-default">
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav">
                                            <li><a href="">arquivos</a></li>
                                            <li><a href="">filiais</a></li>
                                            <li><a href="">vendedores</a></li>
                                        </ul>
                                    </div>
                                </nav -->

                            </div>
                            <div class="col-md-2">
                                <div class="top-bar-right">
                                    @if (Auth::guest())
                                        <a href="{{ url('/auth/login') }}">Login</a>
                                    @else
                                    <div class="logged-user">
                                        <div class="btn-group">
                                            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown" href="#">
                                                <span class="name">{{ Auth::user()->name }}</span>
                                                <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ url('/auth/register') }}">
                                                        <i class="fa fa-user"></i>
                                                        <span class="text">Novo Usuário</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ url('/auth/logout') }}">
                                                        <i class="fa fa-power-off"></i>
                                                        <span class="text">Sair</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Laravel</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/') }}">Home</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav -->


    <footer class="footer">
        © 2015-2016 Todos os direitos reservados
    </footer>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('/js/jquery.mask.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('/bower_components/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('/bower_components/jquery-file-upload/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('/js/custom.js') }}"></script>
</body>
</html>
