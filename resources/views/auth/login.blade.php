@extends('guest')

@section('content')
<div class="wrapper full-page-wrapper page-auth page-login text-center">

    @if (count($errors) > 0)
    <div class="alert alert-danger top-general-alert" style="display: block;">
        <span><strong>Opa!</strong> Houve um erro no seu acesso:</span>
        <button class="close" type="button">×</button><br />

            @foreach ($errors->all() as $error)
                - {{ $error }}<br />
            @endforeach
    </div>
    @endif

    <div class="inner-page">
        <div class="logo">
            <a href="{{ url('/') }}">
                <img alt="" src="{{ asset('/images/logo.png') }}">
            </a>
        </div>

        <div class="separator"><span><strong>Ibi</strong>App</span></div>

        <div class="login-box center-block">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <p class="title">Acesse com seu e-mail / senha</p>

                <div class="form-group">
                    <label class="control-label sr-only" for="username">Usuário</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <input id="username" name="email" class="form-control" type="email" placeholder="e-mail" value="{{ old('email') }}">
                            <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label sr-only" for="password">Senha</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <input id="password" name="password" class="form-control" type="password" placeholder="senha">
                            <span class="input-group-addon">
                            <i class="fa fa-lock"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <label class="fancy-checkbox">
                    <input type="checkbox" name="remember">
                    <span>Lembrar meus dados</span>
                </label>

                <button class="btn btn-custom-primary btn-lg btn-block btn-auth">
                    <i class="fa fa-arrow-circle-o-right"></i>
                    Login
                </button>

                <a class="btn btn-link" href="{{ url('/password/email') }}">Esqueceu a senha?</a>
            </form>
        </div>
    </div>

</div>

@endsection
