@extends('guest')

@section('content')
<div class="wrapper full-page-wrapper page-auth page-login text-center">

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger top-general-alert" style="display: block;">
            <span><strong>Opa!</strong> Houve um erro na sua ação:</span>
            <button class="close" type="button">×</button><br />

            @foreach ($errors->all() as $error)
                - {{ $error }}<br />
            @endforeach
        </div>
    @endif

    <div class="inner-page">
        <div class="logo">
            <a href="{{ url('/') }}">
                <img alt="" src="{{ asset('/images/logo.png') }}">
            </a>
        </div>

        <div class="separator"><span><strong>Ibi</strong>App</span></div>

        <div class="login-box center-block">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="control-label sr-only" for="username">E-mail</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <input id="username" class="form-control" type="email" placeholder="e-mail" value="{{ old('email') }}">
                            <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <button class="btn btn-custom-primary btn-lg btn-block btn-auth">
                    <i class="fa fa-arrow-circle-o-right"></i>
                    Resetar a Senha
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
