@extends('app')

@section('content')
    <div class="content content-wrapper">
        @include('files.form', ['actionType' => 'update'])
    </div>
@endsection