@if ($errors->has())
<div class="alert alert-info">
    @foreach($errors->all() as $error)
        {{ $error }} <br />
    @endforeach
</div>
@endif

<div class="widget">
    <div class="widget-header">
        <h3>
            <i class="fa fa-edit"></i>
            Arquivo
        </h3>
    </div>

    <div class="widget-content">
        @if($actionType == 'add')
        {!! Form::open([
            'method'    => 'POST',
            'action'    => ['ArquivoController@store'],
            'class'     => 'form-horizontal',
            'files'     =>  true
        ]) !!}
        @else
        {!! Form::model($arquivo, [
            'method'    => 'PATCH',
            'action'    => ['ArquivoController@update', $arquivo->id],
            'class'     => 'form-horizontal',
            'files'     => true
        ]) !!}
        @endif
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name">Nome</label>
                <div class="col-sm-9">
                    {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="description">Descrição</label>
                <div class="col-sm-9">
                    {!! Form::text('description', Input::old('description'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="filial_id">Filial</label>
                <div class="col-sm-9">
                    {!! Form::select('filial_id', $filials, null, ['class' => 'multiselect']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="ativo">Ativo</label>
                <div class="col-sm-9">
                    {!! Form::checkbox('ativo', '1', Input::old('ativo')) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="arquivo">Arquivo</label>
                <div class="col-sm-9">
                    {!! Form::file('arquivo', array('class' => 'name')) !!}
                </div>
            </div>
            <br>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    {!! Form::submit('Salvar Arquivo', ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
