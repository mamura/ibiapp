@extends('app')

@section('content')
    <div class="col-md-12 content-wrapper">
        <div class="row">
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            @include('partials.menu_interno')
        </div>

        <div class="content">
            <div class="main-header">
                <h2>Arquivos</h2>
            </div>

            <div class="main-content">
                <div class="bottom-30px">
                    <a class="btn btn-primary" href="{{ URL::to('arquivos/create') }}">
                        <i class="fa fa-plus-square"></i>
                        Novo Arquivo
                    </a>
                </div>
                <div class="table-responsive">
                    <table class="table colored-header datatable project-list">
                        <thead>
                            <th>Id</th>
                            <th>Arquivo</th>
                            <th>Nome</th>
                            <th>Descrição</th>
                            <th>Ativo</th>
                            <th colspan="2">Ações</th>
                        </thead>

                        <tbody>
                            @foreach($files as $f)
                            <tr>
                                <td>{{ $f->id }}</td>
                                <td>
                                    <a href="{{ $f->pdf }}" target="_blank">
                                        <img src="{{ $f->img }}" width="100px">
                                    </a>
                                </td>
                                <td>{{ $f->name }}</td>
                                <td>{{ $f->description }}</td>
                                <td>{{ $f->ativo }}</td>
                                <td>
                                    <a href="{{ URL::to('arquivos/'.$f->id.'/edit') }}">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </a>
                                </td>
                                <td>
                                    {!! Form::open(array('url' => 'arquivos/' . $f->id, 'class' => 'pull-right')) !!}
                                        {!! Form::hidden('_method', 'DELETE') !!}
                                        <button class="glyphicon glyphicon-trash" type="submit">
                                            <i class="icon-circle-arrow-right icon-large"></i>
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection()