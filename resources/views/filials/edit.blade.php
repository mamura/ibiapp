@extends('app')

@section('content')
    <div class="content content-wrapper">
        @include('filials.form', ['actionType' => 'update'])
    </div>
@endsection