@if ($errors->has())
<div class="alert alert-info">
    @foreach($errors->all() as $error)
        {{ $error }} <br />
    @endforeach
</div>
@endif

<div class="widget">
    <div class="widget-header">
        <h3>
            <i class="fa fa-edit"></i>
            Filial
        </h3>
    </div>

    <div class="widget-content">
        @if($actionType == 'add')
        {!! Form::open([
            'method'    => 'POST',
            'action'    => ['FilialController@store'],
            'class'     => 'form-horizontal'
        ]) !!}
        @else
        {!! Form::model($filial, [
        'method'    => 'PATCH',
        'action'    => ['FilialController@update', $filial->id],
        'class'     => 'form-horizontal'
        ]) !!}
        @endif
            <div class="form-group">
                <label class="col-sm-3 control-label" for="code">Código</label>
                <div class="col-sm-9">
                    {!! Form::text('code', Input::old('code'), ['class' => 'form-control number']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="code">Nome</label>
                <div class="col-sm-9">
                    {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    {!! Form::submit('Salvar Filial', ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
