@extends('app')

@section('content')
    <div class="col-md-12 content-wrapper">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            @include('partials.menu_interno')

        </div>

        <div class="content">
            <div class="main-header">
                <h2>Filiais</h2>
            </div>

            <div class="main-content">
                <div class="bottom-30px">
                    <a class="btn btn-primary" href="{{ URL::to('filiais/create') }}">
                        <i class="fa fa-plus-square"></i>
                        Nova Filial
                    </a>
                </div>
                <div class="table-responsive">
                    <table class="table colored-header datatable project-list">
                        <thead>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Nome</th>
                            <th colspan="2">Ações</th>
                        </thead>

                        <tbody>
                            @foreach($filials as $f)
                            <tr>
                                <td>{{ $f->id }}</td>
                                <td>{{ $f->code }}</td>
                                <td>{{ $f->name }}</td>
                                <td>
                                    <a href="{{ URL::to('filiais/'.$f->id.'/edit') }}">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </a>
                                </td>
                                <td>
                                    {!! Form::open(array('url' => 'filiais/' . $f->id, 'class' => 'pull-right')) !!}
                                        {!! Form::hidden('_method', 'DELETE') !!}
                                        <button class="glyphicon glyphicon-trash" type="submit">
                                            <i class="icon-circle-arrow-right icon-large"></i>
                                        </button>
                                    {!! Form::close() !!}

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection()