@extends('app')

@section('content')
<div class="container dashboard">
	<div class="row">
		<div class="col-md-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ url('arquivos') }}">
                            <div class="panel-stat bg-danger">
                                <div class="stat-icon">
                                    <i class="fa fa-file-text-o fa-3x"></i>
                                </div>
                                <div class="pull-right text-right">
                                    <h2 id="userCount">{{ $cArquivos }}</h2>
                                    <h5>Arquivos Carregados</h5>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a href="{{ url('filiais') }}">
                            <div class="panel-stat bg-info">
                                <div class="stat-icon">
                                    <i class="fa fa-share-alt fa-3x"></i>
                                </div>
                                <div class="pull-right text-right">
                                    <h2 id="userCount">{{ $cFiliais }}</h2>
                                    <h5>Filiais Cadastradas</h5>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a href="{{ url('vendedores') }}">
                            <div class="panel-stat bg-warning">
                                <div class="stat-icon">
                                    <i class="fa fa-users fa-3x"></i>
                                </div>
                                <div class="pull-right text-right">
                                    <h2 id="userCount">{{ $cVendedores }}</h2>
                                    <h5>Vendedores Cadastrados</h5>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
		</div>
	</div>
</div>
@endsection
