<div class="col-md-4"></div>
<div class="col-md-8">
    <div class="top-content">
        <ul class="list-inline quick-access">
            <li>
                <a href="{{ url('arquivos') }}">
                    <div class="quick-access-item bg-color-red">
                        <i class="fa fa-file-text-o"></i>
                        <h5>ARQUIVOS</h5>
                        <em>{{$totalArquivos}} arquivos cadastrados</em>
                    </div>
                </a>
            </li>

            <li>
                <a href="{{ url('filiais') }}">
                    <div class="quick-access-item bg-color-blue">
                        <i class="fa fa-share-alt"></i>
                        <h5>FILIAIS</h5>
                        <em>{{$totalFiliais}} filiais ativas</em>
                    </div>
                </a>
            </li>

            <li>
                <a href="{{ url('vendedores') }}">
                    <div class="quick-access-item bg-color-yellow">
                        <i class="fa fa-users"></i>
                        <h5>VENDEDORES</h5>
                        <em>{{$totalVendedores}} vendedores cadastrados</em>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>