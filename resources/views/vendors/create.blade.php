@extends('app')

@section('content')
    <div class="content content-wrapper">
        @include('vendors.form', ['actionType' => 'add'])
    </div>
@endsection