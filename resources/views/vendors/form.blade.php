@if ($errors->has())
<div class="alert alert-info">
    @foreach($errors->all() as $error)
        {{ $error }} <br />
    @endforeach
</div>
@endif

<div class="widget">
    <div class="widget-header">
        <h3>
            <i class="fa fa-edit"></i>
            Vendedor
        </h3>
    </div>

    <div class="widget-content">
        @if($actionType == 'add')
        {!! Form::open([
            'method'    => 'POST',
            'action'    => ['VendorController@store'],
            'class'     => 'form-horizontal'
        ]) !!}
        @else
        {!! Form::model($vendor, [
        'method'    => 'PATCH',
        'action'    => ['VendorController@update', $vendor->id],
        'class'     => 'form-horizontal'
        ]) !!}
        @endif
            <div class="form-group">
                <label class="col-sm-3 control-label" for="code">Código</label>
                <div class="col-sm-9">
                    {!! Form::text('code', Input::old('code'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="code">Nome</label>
                <div class="col-sm-9">
                    {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="code">Filial</label>
                <div class="col-sm-9">
                    {!! Form::select('filial_id', $filials, null, ['class' => 'multiselect']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    {!! Form::submit('Salvar Vendedor', ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
