@extends('app')

@section('content')
    <div class="col-md-12 content-wrapper">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            @include('partials.menu_interno')
        </div>

        <div class="content">
            <div class="main-header">
                <h2>Vendedores</h2>
            </div>

            <div class="main-content">
                <div class="bottom-30px">
                    <a class="btn btn-primary" href="{{ URL::to('vendedores/create') }}">
                        <i class="fa fa-plus-square"></i>
                        Novo Vendedor
                    </a>
                </div>

                <div class="widget">
                    {!! Form::open(array('method' => 'GET', 'action' => ['VendorController@index'])) !!}
                    <div class="widget-header">
                        <h3>
                            <i class="glyphicon glyphicon-filter"></i>
                            Filtros
                        </h3>

                        <div class="btn-group widget-header-toolbar">
                            <div class="btn-group control-inline toolbar-item-group">
                                <button class="glyphicon glyphicon-search" type="submit">
                                    <i class="icon-circle-arrow-right icon-large"></i>
                                </button>
                            </div>
                        </div>

                        <div class="btn-group widget-header-toolbar">
                            <div class="btn-group control-inline toolbar-item-group">
                                {!! Form::select('filial_id', array_merge(['' => 'Filial'],$filials), null, ['class' => 'multiselect']) !!}
                            </div>
                        </div>

                        <div class="btn-group widget-header-toolbar">
                            <div class="control-inline toolbar-item-group">
                                {!! Form::text('name', Input::get('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                        </div>

                        <div class="btn-group widget-header-toolbar">
                            <div class="btn-group control-inline toolbar-item-group">
                                {!! Form::text('code', Input::get('code'), ['class' => 'form-control number', 'placeholder' => 'Código']) !!}
                            </div>
                        </div>
                    </div>
                      {!! Form::close() !!}
                    <div class="widget-content">
                        <div class="table-responsive">
                            <table class="table colored-header datatable project-list">
                                <thead>
                                <th>Id</th>
                                <th>Código</th>
                                <th>Nome</th>
                                <th colspan="2">Ações</th>
                                </thead>

                                <tbody>
                                @foreach($vendors as $v)
                                    <tr>
                                        <td>{{ $v->id }}</td>
                                        <td>{{ $v->code }}</td>
                                        <td>{{ $v->name }}</td>
                                        <td>
                                            <a href="{{ URL::to('vendedores/'.$v->id.'/edit') }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </td>
                                        <td>
                                            {!! Form::open(array('url' => 'vendedores/' . $v->id, 'class' => 'pull-right')) !!}
                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            <button class="glyphicon glyphicon-trash" type="submit">
                                                <i class="icon-circle-arrow-right icon-large"></i>
                                            </button>
                                            {!! Form::close() !!}

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="paginacao">
                                {!! $vendors->render() !!}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection()