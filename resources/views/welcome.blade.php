<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/home.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/flaticon.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{ asset('/js/home.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
	<body>
        <div id="preloader">
            <img alt="" src="{{ asset('/images/logo.png') }}">
        </div>
        <div class="wrap">
            <div id="logo">
                <a href="#">
                    <img alt="" src="{{ asset('/images/logo.png') }}">
                </a>
            </div>
        </div>

        <div id="fullpage" class="fullpage-wrapper" style="height: 100%; position: relative; top: 0px;">
            <div class="wrap">
                <div class="section-image">
                    <img alt="" src="{{ asset('/images/1.jpg') }}" />
                </div>
                <div id="hand"></div>
            </div>

            <div id="section0" class="section fp-section active fp-table" style="height: 279px;" data-anchor="Home">
                <div class="fp-tableCell" style="height: 279px;">
                    <div class="wrap">
                        <div class="box">
                            <h1>
                                Baixe aqui o mais novo
                                <strong>Ibi</strong>App
                            </h1>
                            <p>
                                Mostre aos seus clientes nossas principais promoções!
                            </p>
                            <a class="simple-button" href="ibiapp.apk">
                                <span class="icon flaticon-download7"></span>
                                Download App
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="container">
			<div class="content">
				<div class="title">Laravel 5</div>
				<div class="quote">{{ Inspiring::quote() }}</div>
			</div>
		</div>
	</body>
</html>
